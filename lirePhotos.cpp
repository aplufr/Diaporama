/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "lirePhotos.h"

lirePhotos::lirePhotos(XMLNode xNode,SDL_Surface *screen)
:xNode(xNode),screen(screen),numeroPhoto(0)
{
}

void lirePhotos::fadeBlack(void)
{
	int valeur=255;
	SDL_Surface *tmp;
	for (valeur=0; valeur < getTransitionLoop(); valeur++)
	{	
		tmp=SDL_DisplayFormat(screen);
		SDL_FillRect(tmp,0,SDL_MapRGB(screen->format,0x00,0x00,0x00));
		SDL_SetAlpha(tmp,SDL_SRCALPHA,valeur);
		SDL_BlitSurface(tmp,0,screen,0);
		SDL_Flip(screen);
		SDL_Delay(10);
		SDL_FreeSurface(tmp);
	}
}
SDL_Surface * lirePhotos::zoomEtNoir(SDL_Surface *image, double zoomx, double zoomy, SDL_Rect pic)
{
	/*zoomSurface (SDL_Surface *src, double zoomx, double zoomy, int smooth);

	Zoomes a 32bit or 8bit 'src' surface to newly created 'dst' surface.
	'zoomx' and 'zoomy' are scaling factors for width and height. If 'smooth' is 1
	then the destination 32bit surface is anti-aliased. If the surface is not 8bit
	or 32bit RGBA/ABGR it will be converted into a 32bit RGBA format on the fly.
	Smoothing (interpolation) flags work only on 32bit surfaces:
	#define SMOOTHING_OFF		0
	#define SMOOTHING_ON		1
	*/

	SDL_Surface *imagePast, *noir;
	//Zoom de l'image
	imagePast=zoomSurface(image,zoomx,zoomy,1);

	noir=SDL_DisplayFormat(screen); // avoir un noir comme l'écran
	SDL_FillRect(noir,0,SDL_MapRGB(screen->format,R,G,B)); //Remplir noir avec du noir
	SDL_SetAlpha(noir,SDL_SRCALPHA,SDL_ALPHA_OPAQUE); //rendre le noir opaque
	SDL_BlitSurface(imagePast,0,noir,&pic); // on rajoute la photo dans noir
	SDL_FreeSurface(imagePast); //on libere imagePast
	//imagePast=SDL_DisplayFormat(noir); //on copie noir (photo + noir) dans imagePast
	//SDL_FreeSurface(noir);
	return noir;
}

void lirePhotos::fadePhoto(SDL_Surface *imageNew)
{
	int valeur;
	int endLoop=getTransitionLoop();
	SDL_Surface *tmp, *imagePast;

	//le fondu est ici
	for (valeur=0 ; valeur < endLoop; valeur+=2 )
	{	
		//Remplir avec la nouvelle image
		tmp=SDL_DisplayFormat(imageNew); // une copie de imageNew
		SDL_SetAlpha(tmp,SDL_SRCALPHA,valeur); //on applique un peut d'alpha
		SDL_BlitSurface(tmp,0,screen,0); //on superpose tmp dans screen

		//Mise à jour de l'affichage
		SDL_Flip(screen);
		SDL_Delay(10);
		//Eviter le OOM
		SDL_FreeSurface(tmp);
	}
}

void lirePhotos::lire(void)
{
	string imgstr;
	SDL_Rect  pic;
	SDL_Surface *image , *imageZoom;
	double zoomx, zoomy;
	XMLNode xPhoto;
	if (validXMLAttrFic())
	{
		xPhoto = xNode.getChildNode("photo",numeroPhoto);
	}
	else
	{
		suivante();
		return;
	}
		
	imgstr = xPhoto.getAttribute("fic");
	//cout << "Chargement image (" << numeroPhoto << ") : " << imgstr << endl;
	image = IMG_Load(imgstr.c_str());
	if(image == NULL)
	{
		cerr << "Erreur avec l'image n°" << numeroPhoto << " : " << imgstr  << endl;
		xPhoto.deleteNodeContent();
		suivante();
		return;
	}
	if (image->w > image->h){
		zoomx = static_cast<double>(W_WIN)/image->w;
		zoomy = zoomx; // facteur de zoom 
		pic.x=0;
		pic.y=(H_WIN - static_cast<int>((image->h * zoomy)))/2; //centrage de l'image 
		pic.w=W_WIN;
		pic.h=static_cast<int>(image->h * zoomy);
//		cerr << "zoomx " << zoomx << "pic" << pic.y << " pic " << pic.w << " pic " << pic.h << endl;
	}
	else{
		zoomy = static_cast<double>(H_WIN)/image->h;
		zoomx = zoomy; // facteur de zoom 
		pic.y=0;
		pic.x=(W_WIN - static_cast<int>((image->w * zoomx)))/2; //centrage de l'image 
		pic.w=H_WIN;
		pic.h=static_cast<int>(image->w * zoomx);
//		cerr << "zoomy " << zoomy << " Apic" << pic.x << " pic " << pic.w << " pic " << pic.h << endl;
	}
		
	imageZoom=zoomEtNoir(image,zoomx,zoomy,pic);
	SDL_FreeSurface(image);
	fadePhoto(imageZoom);
	SDL_BlitSurface(imageZoom,NULL,screen,0);
	SDL_Flip(screen);
	//netoyage
	SDL_FreeSurface(imageZoom);
	SDL_FreeSurface(screen);

}
void lirePhotos::precedente(void)
{
	if(numeroPhoto>0)
		numeroPhoto--;
}
int lirePhotos::suivante(void)
{
	numeroPhoto++; //pour la prochaine photo
	if (numeroPhoto > getNbPhoto())
	{
		fadeBlack(); // fonction qui devrait basculer l'écran vers le blackout
		SDL_Delay(200);
		numeroPhoto = 0;
		return 1;
	}
	else
	{
		return 0;
	}
}
void lirePhotos::derniere(void)
{
	numeroPhoto=getNbPhoto();
}
void lirePhotos::debut(void)
{
	numeroPhoto=0;
}
int lirePhotos::getNbPhoto(void)
{

	return xNode.nChildNode("photo")-1;
}

int lirePhotos::getDelay(void)
{
	if(validXMLAttrFic() && xNode.getChildNode("photo",numeroPhoto).isAttributeSet("delay"))
	{
		int delay = atoi(xNode.getChildNode("photo",numeroPhoto).getAttribute("delay")) / 100 ;
		if (delay<3)
			delay=3;
		return delay;
	}
	return 5; //delais par defaut si l'XML n'est pas bon !
}

int lirePhotos::getTransitionLoop(void)
{
	return 50;
	//return atoi(xNode.getChildNode("transition").getAttribute("loop"));
}
bool lirePhotos::validXMLAttrFic(void)
{
	XMLNode xPhoto = xNode.getChildNode("photo",numeroPhoto);
	if (xPhoto.isEmpty())
	{
		cerr << "Erreur balise photo : " << numeroPhoto << endl;
		xPhoto.deleteNodeContent();
		return false;
	}
	if (!xPhoto.isAttributeSet("fic"))
	{
		cerr << "Erreur balise photo : " << numeroPhoto << " pas d'attribut fic !" << endl;
		xPhoto.deleteNodeContent();
		return false;
	}
	return true;
}
