/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "main.h"
using namespace std;


int main(int argc, char *argv[])
{
	cout << "Vous utilisez " << PACKAGE_NAME << " dans la version " << PACKAGE_VERSION << endl;
	cout << "Merci de reporter les bugs ici : " << PACKAGE_BUGREPORT << endl;
	cout << "Ce logiciel à été écrit par Aymeric PETIT (MulX/APLU) (http://aplu.fr)" << endl;
	cout << "Avec le cahier des charges de GEP31 " << endl;
	cout << "This program is licenced under GPLv3" << endl << endl;
	cout << "But the library used to parse XML file is under the AFPL" << endl;
	cout << "The xmlParser is (C) by Dr. Ir. Frank Vanden Berghen" << endl;
	cout << "Please go here for more information: http://www.applied-mathematics.net/tools/xmlParser.html" << endl;
	cout << endl << endl;
	XMLNode xMainNode = XMLNode::openFileHelper("./diapo.xml","DIAPO"); //ouverture du fichier xml

	/* Generation du XML patern
	XMLNode xMainNode = XMLNode::createXMLTopNode("DIAPO");
	xMainNode.addChild("audio").addAttribute("fic","musiques/mus.mp3");
	xMainNode.addChild("photo").addAttribute("fic","photo/20090528_0237.JPG");
	xMainNode.getChildNode("photo").addAttribute("delay","2000");
	xMainNode.writeToFile("diapo.xml");
	*/

	SDL_Event event; //gestion des evenements
	SDL_Surface *screen; //Pointeur vers l'ecran
	int done = 0,delay=0;
	bool pause=false, fullscreen=false;
	Mix_Music *music; //Musiques
	bool MUSIQUES_INIT=true;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		cerr << "Impossible d'initialiser SDL : " << SDL_GetError() << endl;
		return 1;
	}
	
	// a definir depuis l'exterieur ?
	if (Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 4096) != 0) // audio_rate, audio_format, audio_channels, audio_buffers 
	{
		cerr << "Impossible d'initialiser l'audio : " << Mix_GetError() << endl;
		MUSIQUES_INIT=false;
		//return 2;
	}
	

	//screen = SDL_SetVideoMode(H_WIN,W_WIN,24,SDL_FULLSCREEN|SDL_DOUBLEBUF); //taille de l'écran xyz
	screen = SDL_SetVideoMode(W_WIN, H_WIN,SDL_VideoModeOK(W_WIN,H_WIN,32,SDL_ANYFORMAT),SDL_ANYFORMAT); //taille de l'écran xyz
	if (screen == NULL)
	{
		cerr << "Impossible d'initialiser la video : " << SDL_GetError() << endl;
		return 3;
	}
	SDL_WM_SetCaption(PACKAGE_STRING,"");

	lireMusiques * mus = lireMusiques::pInst(xMainNode,music, MUSIQUES_INIT); //ma fonction musique
	mus->lire();



	lirePhotos photo = lirePhotos(xMainNode, screen);
	
	while (!done)
	{
		if(!pause){
			photo.lire();
		}

		for(delay=0 ; delay < photo.getDelay() &&  done ==0 ; delay++)
		{
			while(SDL_PollEvent(&event)) {
				switch(event.type) {
					case SDL_QUIT:
						done = 1;
						break;
					case SDL_KEYDOWN:
						break;
					case SDL_KEYUP:
						switch(event.key.keysym.sym){
							case SDLK_f:
								if(fullscreen){
									fullscreen=false;
									SDL_SetVideoMode(W_WIN,H_WIN,SDL_VideoModeOK(W_WIN,H_WIN,32,SDL_ANYFORMAT),SDL_ANYFORMAT); 
									photo.lire();
								}
								else{
									fullscreen=true;
									if(SDL_VideoModeOK(W_WIN,H_WIN,32,SDL_FULLSCREEN|SDL_DOUBLEBUF)){ //on test si c'est possible
										SDL_SetVideoMode(W_WIN,H_WIN,SDL_VideoModeOK(W_WIN,H_WIN,32,SDL_FULLSCREEN|SDL_DOUBLEBUF),SDL_FULLSCREEN|SDL_DOUBLEBUF);
									}
									else
									{
										fullscreen=false; //et ben non ..
									}
									photo.lire();
								}
								break;
							case SDLK_ESCAPE:
								done = 1;
								break;
							case SDLK_UP:
								photo.debut();
								photo.lire();
								break;
							case SDLK_m:
								photo.derniere();
								photo.lire();
								break;
							case SDLK_SPACE:
							case SDLK_DOWN:
								if(pause)
								{
									//cerr << "pause OFF" << endl;
									pause=false;
								}
								else
								{
									//cerr << "pause ON" << endl;
									pause=true;
								}
								break;
							case SDLK_RIGHT:
								done = photo.suivante();
								photo.lire();
								break;
							case SDLK_LEFT:
								photo.precedente();
								photo.lire();
						}
						break;
					}
			}
			SDL_Delay(100);
		}
		//cerr << "pause=" << pause << " done=" << done << endl;
		if(!pause && !done)
		{
			/*done =*/ photo.suivante();
		}
	}
	SDL_FreeSurface(screen);
	delete mus;
	mus=NULL;
	SDL_Quit();
	return 0;
}
