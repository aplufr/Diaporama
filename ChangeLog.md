Version 1.0.4
=============

Mise à jour du parseur XML

Version 1.0.3
==============

Verification du fichier XML avant chargement d'une photo (balise photo ainsi que les attributs correspondants)

Verification du fichier XML avant chargement d'une musique (balise audio)

Sat, 31 Oct 2009 14:34:59 +0100

Version 1.0.2
=============

Correction d'un problème mémoire dans la classe lireMusiques

Correction d'un problème de confidentialité

Wed, 28 Oct 2009 09:47:47 +0100 
