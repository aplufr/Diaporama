/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef _SCREENDEF_
#define _SCREENDEF_
/*
#define H_WIN	800
#define W_WIN	800
#define H_SLIDE	600
#define W_SLIDE	800
*/
#define H_WIN	768
#define W_WIN	1024
#define H_SLIDE	768
#define W_SLIDE	1024

#endif // _SCREENDEF_
