/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef _MAIN_H_
#define	_MAIN_H_

//fichier générer
#include "config.h"

#include <ostream> //cerr/cout

#ifndef HAVE_SDL_SDL_H
#error "Pas de fichier SDL/SDL.h"
#else
#include <SDL/SDL.h>

#ifndef HAVE_SDL_SDL_MIXER_H
#error "Pas de fichier SDL/SDL_mixer.h"
#else
#include <SDL/SDL_mixer.h>
#endif //HAVE_SDL_SDL_MIXER_H


#endif //HAVE_SDL_SDL_H

#include "xmlParser.h"
#include "lireMusiques.h"
#include "lirePhotos.h"
#include "defineScreen.h"
#endif //_MAIN_H_
