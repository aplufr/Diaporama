/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef _MUSIQUES_
#define _MUSIQUES_
//fichier généré
#include "config.h"

#include <string> //pour string
#include <iostream> //pour cerr et cout

#ifndef HAVE_SDL_SDL_H
#error "Pas de fichier SDL/SDL.h"
#else
#include <SDL/SDL.h>

#ifndef HAVE_SDL_SDL_MIXER_H
#error "Pas de fichier SDL/SDL_mixer.h"
#else
#include <SDL/SDL_mixer.h>
#endif // HAVE_SDL_SDL_MIXER_H

#endif // HAVE_SDL_SDL_H

#include "xmlParser.h"

using namespace std;

class lireMusiques
{
public: 
	static lireMusiques *pInst(XMLNode xNode, Mix_Music *music, bool audioStatus);
	void lire(void);
	~lireMusiques();
private:
	void suiv(void);
	XMLNode xNode;
	static void fini(void);
	lireMusiques(XMLNode xNode,Mix_Music *music, bool audioStatus);
	bool MUSIQUES_INIT;
	int getNbMusic(void);
	int numeroMusiques;
	Mix_Music *music;
	static  lireMusiques * instance  ;

};


#endif // _MUSIQUES_
