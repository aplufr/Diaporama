/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef _PHOTOS
#define _PHOTOS
//fichier généré

#include "config.h"

#include <string>
#include <iostream>

#ifndef HAVE_SDL_SDL_H
#error "Pas de fichier SDL/SDL.h"
#else
#include <SDL/SDL.h>

#ifndef HAVE_SDL_SDL_ROTOZOOM_H
#error "Pas de fichier SDL/SDL_rotozoom.h"
#else
#include <SDL/SDL_rotozoom.h>
#endif // HAVE_SDL_SDL_ROTOZOOM_H

#ifndef HAVE_SDL_SDL_IMAGE_H
#error "Pas de fichier SDL/SDL_image.h"
#else
#include <SDL/SDL_image.h>
#endif // HAVE_SDL_SDL_IMAGE_H

#endif // HAVE_SDL_SDL_H

#include "xmlParser.h"
#include "defineScreen.h"

#define R 0xff
#define G 0xff
#define B 0xff


using namespace std;
class lirePhotos
{
public:
	lirePhotos(XMLNode xNode,SDL_Surface *screen);
	void lire(void);
	void debut(void);
	void derniere(void);
	int suivante(void);
	void precedente(void);
	int getDelay(void);
private:
	int getNbPhoto(void);
	int getTransitionLoop(void);
	void fadeBlack(void);
	void fadePhoto (SDL_Surface *imageNew);
	SDL_Surface * zoomEtNoir(SDL_Surface *image, double zoomx, double zoomy, SDL_Rect pic);
	XMLNode xNode;
	SDL_Surface *screen;
	int numeroPhoto;
	bool validXMLAttrFic(void);
};

#endif // _PHOTOS
