/**
	This file is part of Diaporama

	Diaporama is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Diaporama is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Diaporama.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "lireMusiques.h"
lireMusiques * lireMusiques::instance=NULL;

lireMusiques::lireMusiques(XMLNode xNode, Mix_Music *music, bool audioStatus)
:xNode(xNode),music(music),numeroMusiques(0),MUSIQUES_INIT(audioStatus)
{
}
lireMusiques * lireMusiques::pInst(XMLNode xNode, Mix_Music *music, bool audioStatus)
{
	if (instance == NULL)
		instance = new lireMusiques(xNode,music,audioStatus);
	return instance;
}

lireMusiques::~lireMusiques()
{
	Mix_FreeMusic(music);
	music=NULL;
}
int lireMusiques::getNbMusic(void)
{
	return xNode.nChildNode("audio")-1;
}

void lireMusiques::suiv(void)
{
	numeroMusiques++;
	if (numeroMusiques > getNbMusic())
		numeroMusiques=0;
	Mix_FreeMusic(music);
	music=NULL;
	lire();
}

void lireMusiques::lire(void)
{
	if (MUSIQUES_INIT) // si il n'y a pas d'audio
	{
		string musstr; //equivalent d'un char * (mais en mieux)
		XMLNode xAudio = xNode.getChildNode("audio",numeroMusiques);
		if (xAudio.isEmpty())
		{
			cerr << "Impossible de trouver la balise audio ! Desactivation de l'audio ! " << endl;
			MUSIQUES_INIT=false;
			return ;
		}
		if (!xAudio.isAttributeSet("fic"))
		{
			cerr << "Erreur XML pour la balise audio n°" << numeroMusiques << endl;
			xAudio.deleteNodeContent();
			suiv();
			return;
		}
		musstr = xAudio.getAttribute("fic"); //attribut fic de la balise "i" audio
		//cerr << "Chargement musiques (" << numeroMusiques << ") : " << musstr << endl;
		music = Mix_LoadMUS(musstr.c_str()); //chargement de la musique
		if (music == NULL)
		{	
			cerr << "Impossible de charger la musiques n°" << numeroMusiques << " fichier : " << musstr << " message d'erreur : " << Mix_GetError() << endl;
			xAudio.deleteNodeContent();
			suiv();
			return;
		}
		Mix_PlayMusic(music,0); // 0 = une lecture
		Mix_HookMusicFinished( &lireMusiques::fini);
	}
}
		
void lireMusiques::fini(void)
{
	instance->suiv();
}
